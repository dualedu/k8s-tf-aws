#!/bin/bash
# This scripts performs postinstall configuraton on deployed VMs.

# Install packages
yum -y install git

# hostname
hostnamectl set-hostname docker --static

# Create User
useradd -s /bin/bash -c "Admin" -m admin
echo "Passw0rd" | passwd --stdin admin
# Set sudo
echo "admin ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
# Deploy SSH keys
mkdir /home/admin/.ssh
cat <<EOF | tee -a /home/admin/.ssh/authorized_keys
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB+FcJU11IIvmuF+YGQTCjjshbAVHWGrbsrovA6bp9C5 Peter Barczi, pety.barczi@gmail.com
EOF
# Set proper permissions
chown -R admin /home/admin/.ssh
chmod 700 /home/admin/.ssh
chmod 600 /home/admin/.ssh/authorized_keys
##

# Adjust SSH
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
systemctl restart sshd
##

# Install Docker
yum -y install docker
systemctl enable docker --now 
usermod -aG docker admin
##

## Motd
unlink /etc/motd
cat <<EOF | tee -a /etc/motd
┌─────────────────────────────────────────────────────────────┐
│┌───────────────────────────────────────────────────────────┐│
││                  Welcome to DualEdu!                      ││
││───────────────────────────────────────────────────────────││
││                                                           ││
││              This is your testing machine                 ││
││                                                           ││
│└───────────────────────────────────────────────────────────┘│
└─────────────────────────────────────────────────────────────┘
EOF
##
