# OUTPUTS
# `terraform output -json | jq .[].value | jq .[]`

output "instance_public_ip-master" {
  value = aws_instance.master.*.public_ip
}
